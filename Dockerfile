FROM python:2.7-slim
WORKDIR /app
ADD . /app
RUN pip install -r requirements.txt
RUN python manage.py migrate
EXPOSE 8000
CMD ["python","manage.py","runserver","0.0.0.0:8000"]


