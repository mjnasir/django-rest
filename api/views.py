import json

from django.http import Http404
from django.http import JsonResponse
from django.http.response import HttpResponseNotFound
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Airport
from api.serializer import AirportSerializer


class AirportsListIANA(APIView):
    renderer_classes = (JSONRenderer, )
    def get(self, request,iata):
            airports = Airport.objects.filter(iata__iexact=iata)
            if len(airports) < 1:
    #             return Response({"status" : 404 , "message" : "Not Found"},status=status.HTTP_404_NOT_FOUND)
                raise Http404
            serializer = AirportSerializer(airports.first())
            return Response(serializer.data)
    
    
class AirportsListByName(APIView):
    renderer_classes = (JSONRenderer, )
    def get(self,request,iata):
        airports = Airport.objects.filter(name__icontains=iata)
        if len(airports) < 1:
#             return Response({"status" : 404 , "message" : "Not Found"},status=status.HTTP_404_NOT_FOUND)
            raise Http404
        serializer = AirportSerializer(airports,many=True)
        return Response(serializer.data)
