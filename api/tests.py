# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal
import json
from json.encoder import JSONEncoder

from django.test import TestCase
from rest_framework.test import APITestCase


def default(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    raise TypeError
# Create your tests here.
class AirportsTest(APITestCase):
    def test_iana_search(self):
        response = self.client.get('/api/airports/iata/lhe/')
        x = {"name":"Alama Iqbal International Airport","iata":"LHE","icao":"OPLA","city":"Lahore","country":"PK-PB","latitude":Decimal('31.52160072'),"longitude":Decimal('74.4036026')}
        self.assertEqual(response.data,x )
