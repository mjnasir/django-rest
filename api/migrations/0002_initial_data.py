'''
Created on Jun 5, 2017

@author: mjnasir
'''

from django.db import migrations
from api.models import Airport
import csv
from django.db import transaction
def save_airports(apps, schema_editor):
    
    for airports_obj in Airport.objects.all():
        airports_obj.delete()
    airports_list = open("airports.csv")
    csv_file = csv.reader(airports_list)
    csv_file.next()
    for row in csv_file:
        try:
            with transaction.atomic():
                if row[13] == "" :
                    continue
                airport = Airport()
                airport.name = row[3]
                airport.iata = row[13]
                airport.city = row[10]
                airport.icao = row[12]
                airport.country = row[9]
                airport.latitude = row[4]
                airport.longitude = row[5]
                airport.save()
        except Exception,e :
            pass
            
        
class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(save_airports),
    ]
