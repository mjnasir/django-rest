'''
Created on Jun 5, 2017

@author: mjnasir
'''
from rest_framework import serializers
from api.models import Airport
class AirportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Airport
        exclude = ('id',)
  