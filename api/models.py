# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Airport(models.Model):
    name = models.CharField(max_length=40)
    iata = models.CharField(max_length=10)
    icao = models.CharField(max_length=10)
    city = models.CharField(max_length=10)
    country = models.CharField(max_length=10)
    latitude = models.DecimalField(max_digits=11,decimal_places=8)
    longitude = models.DecimalField(max_digits=11,decimal_places=8)
