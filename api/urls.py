'''
Created on Jun 5, 2017

@author: mjnasir
'''
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from api.views import AirportsListIANA,AirportsListByName

urlpatterns = [
    url(r'^iata/(?P<iata>[\w\-]+)/$', AirportsListIANA.as_view()),
    url(r'^name/(?P<iata>[\w\-]+)/$', AirportsListByName.as_view()),
]

