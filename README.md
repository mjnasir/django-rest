After getting clone do the following steps.
 
1. Build the docker image(sudo docker build -t <image_name> .).

2. Run the built image(sudo docker run -p <port_to_map>:8000 <image_name>).

Server will be listening to the <port_to_map> given.

Exposed EndPoints are:

GET /api/airports/iata/<iata_code>


GET /api/airports/name/<name>

Note: As mentioned in the email to mention new things if I have used. I have used docker and Django-rest-framework for the first time. Before I used to use tasty-pie