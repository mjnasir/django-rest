'''
Created on Jun 5, 2017

@author: mjnasir
'''
import json

from django.http.response import HttpResponseNotFound


def handler404(request):
    return HttpResponseNotFound(json.dumps({"status" : 404 , "detail" : "Not Found"}),content_type="application/json")
def handler500(request):
    return HttpResponseServerError(json.dumps({"status" : 500 , "detail" : "Internal Error"}),content_type="application/json")
